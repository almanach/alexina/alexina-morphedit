#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use CGI qw/:standard Vars/;
use DBI;
use Encode;
use strict;

my %form = Vars();

my  $lang = $form{lang} || "";
my $db = $form{db} || "$lang.dat";
my $firstid = $form{firstid} || 1;
my $entries_by_page = $form{entries} || 25;
my $filename = $form{filename} || "";
my $prevfilename = $form{prevfilename} || "";
my $showsavedentries = $form{showsavedentries} || 0;

if ($filename ne $prevfilename) {$firstid = 1}

print "Content-type: text/html; charset=utf-8\n\n";
print start_html(-title => "Alexina morphological validation interface", -encoding => "UTF-8");

my $pwd=$0;
$pwd=~s/^(.*)\/.*/$1/;
my $dbh;
my $sth;

my %color;

print <<END;
<body>
<form method="GET" action="index.pl" name="form">
END

my ($bool,$bg,$red,$yellow,$green,$orange,$blue);

if ($lang eq "") {
  print "<h1>Database selection</h1>\n";
  print "<select name='lang'>\n";
  for my $l (<$pwd/*.dat>) {
    $l =~ s/^.*\///;
    $l =~ s/\.dat$//;
    print "<option value='$l'>$l</option>\n";
  }
  print "</select>\n";
  print "<input type=submit value='OK'/><br/>";
} else {
  $dbh = DBI->connect("dbi:SQLite:$pwd/$db", "", "", {RaiseError => 0, AutoCommit => 1}) || die "Could not connect (pwd=$pwd, \$0 = $0)";

  print STDERR "$pwd/$db\n";

  $sth = $dbh->prepare("insert into validations (id,login,date,status) values (?,?,?,?);");
  my @state;
  my $saved_items = 0;
  my $remote_user = $ENV{REMOTE_USER} || "(unknown)";
  for (keys %form) {
    if (/^([RYGOFBL][a-z]?)(\d+)$/) {
      $state[$2] = "$1";
      $sth->execute($2,$remote_user,time,$1);
      print STDERR "\$sth->execute($2,$remote_user,time,$1);\n";
      $saved_items++;
    }
  }
  $sth->finish;

  my $direction;
  my $size;
  my $cellpadding;
  if ($lang =~ /^(fa|ckb)/) {
    $direction = ' dir="RTL"';
    $size = "18";
    $cellpadding = "";
  } else {
    $direction = "";
    $size = "12";
    $cellpadding = " cellpadding=4";
  }
  if ($filename eq "") {
    print "<h1>File selection</h1>\n";
  } else {
    print "<h1>Validating file $filename</h1>\n";
    print "Changing file:\n";
  }
  print "<select name='filename'>\n";
  $sth = $dbh->prepare("select distinct file from mslexicon;");
  $sth->execute();
  while (my $fn = $sth->fetchrow) {
    print "<option value='$fn' ";
    if ($filename eq $fn) {
      print "selected";
    }
    print ">$fn</option>\n";
  }
  $sth->finish();
  print "</select>\n";
  print "<input type=hidden name='lang' value='$lang'/>\n";
  print "<input type=hidden name='entries' value='$entries_by_page'/>\n";
  if ($filename eq "") {
    print "<input type=submit value='OK'/><br/>";
  } else {
    print "<input type=submit value='Save and go to file'/><br/>";
    print "(successfuly saved $saved_items validation tokens by user $remote_user)<br/>\n";
    print "<table><tbody><tr>";
    print "<td>Color code:</td><td/>";
    print "<td style='background-color:#FF5555;'>Invalid lemma</td>";
    print "<td style='background-color:#FF9933;'>Invalid category</td>";
    print "<td style='background-color:#FFFF00;'>Valid category, invalid subcat or inflection</td>";
    print "<td style='background-color:#66FF66;'>Fully valid entry</td>";
    print "<td style='background-color:#66FFFF;'>Familiar language (therefore, will be ignored for now)</td>";
    print "<td style='background-color:#888888;'>(don't validate now, keep for later)</td>";
    print "</tr></table></tbody>\n";
    if ($filename eq "N.ilex" || $filename eq "ADJ.ilex") {
      print "Yellow cells including a letter indicate that the inflection is incorrect because of the sound of the final character of the lemma: please check the box next to the letter corresponding to the correct pronunciation of the final character<br/>\n";
    }
    print "<br/>\n";
    print "<button onclick='document.form.submit()'>Save</button>";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n";
    print "<button onclick='document.form.firstid.value=$firstid-$entries_by_page;document.form.submit()'>&lt; Save and Prev</button>" unless $firstid == 1;
    print "<button onclick='document.form.firstid.value=$firstid+$entries_by_page;document.form.submit()'>Save and Next &gt;</button><br/>";
    if ($showsavedentries == 0) {
      print "<button onclick='document.form.showsavedentries.value=1;document.form.submit()'>Show entries saved for later</button><br/>";
    } else {
      print "<button onclick='document.form.showsavedentries.value=0;document.form.submit()'>Show standard entries</button><br/>";
    }
    print <<END;
<input type=hidden name='firstid' value='$firstid'/>
<input type=hidden name='prevfilename' value='$filename'/>
<input type=hidden name='showsavedentries' value='$showsavedentries'/>
<table$cellpadding><tbody>
END
    my $validations_sth = $dbh->prepare("select count(*) from validations where id == ? and  login == ? and status != 'L';");
    my $later_sth = $dbh->prepare("select count(*) from validations where id == ? and  login == ? and status = 'L';");
    $sth = $dbh->prepare("select id,canform,explid,inflclass,inflforms,syntinfos,weight from mslexicon where file == '$filename' and id >= $firstid order by id;");
    $sth->execute();
    my @columns = ("R","O","Y","G","B","L");
    if ($lang eq "fa" && ($filename eq "N.ilex" || $filename eq "ADJ.ilex")) {
      @columns = ("R","O","Yu","Yv","Yo","Ye","Yh","Yi","Yy","Y","G","B","L");
    } elsif ($lang eq "mlt" && ($filename =~ /^[CV]+(?:-val)?\.ilex$/)) {
      @columns = ("R","O","YpfvV","YipfvV","YFV","YmissingFV","YirregImp","Ydefective","Yother","G","L");
    }
    my $entries_nb = 0;
    my $shown_entries_nb = 0;
    my $entries = "";
    my %shown_entries = ();
    while (my @row = $sth->fetchrow_array) {
      $entries_nb++;
#      next if ($entries_nb < $firstid);
      for my $i (0..$#row) {
	$row[$i] = Encode::decode('utf8',$row[$i]);
      }
      next if ($row[4] eq "");
      next if ($row[5] =~ /###VAL### G/);
      next if ($row[5] =~ /###VAL###CORR /);
      next if ($lang eq "fa" && $row[6] >= 107 && $filename ne "N.ilex" && $filename ne "ADJ.ilex" && $row[5] !~ /###VAL### [^G]:~/);
      $validations_sth->execute($row[0],$remote_user);
      next if ($validations_sth->fetchrow_array);
      $later_sth->execute($row[0],$remote_user);
      if ($showsavedentries == 1) {
	next unless ($later_sth->fetchrow_array);
      } else {
	next if ($later_sth->fetchrow_array);
      }
      $shown_entries{$row[0]} = 1;
      $shown_entries_nb++;
      $bool = 1-$bool;
      set_colors ($bool);
      $entries .= "<tr bgcolor='$bg' onmouseover='this.style.background=\"lightblue\";' onmouseout='this.style.background=\"$bg\";'><td><sup>$row[0]</sup></td>";
      $row[4] =~ s/^(.+?)([،,()]|$)/<b>\1<\/b>\2/;
      $row[4] =~ s/(\([^\(\)]+\))/<span style=\"font-size:10pt;\"><sup>\1<\/sup><\/span>/g;
      $entries .= "<td $direction style=\"font-size:".$size."pt;\">$row[4]</td>";
      for my $type (@columns) {
	my $checkboxtype .= "onClick='";
	for my $othertype (@columns) {
	  next if $type eq $othertype;
	  $checkboxtype .= "document.form.$othertype$row[0].checked=false;";
	}
	my $celltype = $checkboxtype;
	$checkboxtype = "type='checkbox' $checkboxtype";
	$checkboxtype .= "this.checked=1-this.checked;";
	$checkboxtype .= "'";
	$celltype .= "this.firstChild.checked=1-this.firstChild.checked;";
	$celltype .= "'";
	$entries .= "<td $celltype style='background-color:".type2color($type).";'>";
	$entries .= "<input $checkboxtype name='$type$row[0]'".($state[$row[0]] eq $type ? " checked" : "").">";
	if ($type =~ /^.(.+)$/) {
	  $entries .= $1;
	}
	print "</td>\n";
      }
      $entries .= "</tr>\n";
      last if ($shown_entries_nb == $entries_by_page);
    }
    $sth->finish;
    print "<tr bgcolor='$bg' onmouseover='this.style.background=\"lightblue\";' onmouseout='this.style.background=\"$bg\";'>\n<td></td>\n";
    print "<td $direction style=\"font-size:".$size."pt;\"><i><b>all</b></i></td>\n";
    for my $type (@columns) {
      my $checkboxtype .= "onClick='";
      for my $entry (keys %shown_entries) {
	$checkboxtype .= "document.form.$type$entry.checked=true;";
	for my $othertype (@columns) {
	  next if $type eq $othertype;
	  $checkboxtype .= "document.form.$othertype$entry.checked=false;";
	}
      }
      for my $othertype (@columns) {
	$checkboxtype .= "document.form.${othertype}ALL.checked=false;";
      }
      my $celltype = $checkboxtype;
      $checkboxtype = "type='checkbox' $checkboxtype'";
      $celltype .= "'";
      print "<td $celltype style='background-color:".type2color($type).";'>";
      print "<input $checkboxtype name='${type}ALL'>";
      if ($type =~ /^.(.+)$/) {
	print $1;
      }
      print "</td>\n";
    }
    print "</tr>\n";
    print $entries;
    print <<END;
</tobdy></table>
<input type=submit value='Save'/>
END
  }
}
print <<END;
</form>
</body>
</head>
END

if ($lang ne "") {
  $sth->finish;
  undef $sth;
  $dbh->disconnect;
}

sub set_colors {
  my $b = shift;
  if ($b == 1) {
    $bg = "#EEEEEE";
    $color{R} = "#FF5555";
    $color{O} = "#FF9933";
    $color{Y} = "#FFFF00";
    $color{G} = "#66FF66";
    $color{B} = "#66FFFF";
    $color{L} = "#888888";
  } else {
    $bg = "white";
    $color{R} = "#FF8888";
    $color{O} = "#FFAA77";
    $color{Y} = "#FFFF44";
    $color{G} = "#AAFFAA";
    $color{B} = "#AAFFFF";
    $color{L} = "#BBBBBB";
  }
}

sub type2color {
  my $t = shift;
  $t =~ /^(.)/;
  return $color{$1};
}
