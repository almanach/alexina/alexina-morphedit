#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use DBI;
use Encode;
use strict;

my $db_val = shift || die "Please provide a validation database file";
my $db_current = shift || die "Please provide a reference database file";

my $dbh = DBI->connect("dbi:SQLite:$db_val", "", "", {RaiseError => 0, AutoCommit => 1}) || die "Could not connect (\$0 = $0)";
my $cdbh = DBI->connect("dbi:SQLite:$db_current", "", "", {RaiseError => 0, AutoCommit => 1}) || die "Could not connect (\$0 = $0)";

my (@val2id, @val2name, @val2date, @val2status, %id2vals);
my $sth = $dbh->prepare("select id,login,date,status from validations;");
$sth->execute();
while (my @row = $sth->fetchrow_array) {
  for my $i (0..$#row) {
    $row[$i] = Encode::decode('utf8',$row[$i]);
  }
  next if ($row[3] eq "L");
  push @val2id, $row[0];
  push @val2name, $row[1];
  push @val2date, $row[2];
  push @val2status, $row[3];
  $id2vals{$row[0]}{$#val2id} = 1;
}
$sth->finish;

my %signature2id;
$sth = $dbh->prepare("select id,canform,explid,inflclass,inflforms from mslexicon;");
$sth->execute();
while (my @row = $sth->fetchrow_array) {
  for my $i (0..$#row) {
    $row[$i] = Encode::decode('utf8',$row[$i]);
  }
  if (defined($id2vals{$row[0]})) {
    $row[1] =~ s/ħ/ح/g;
    $row[1] =~ s/ẓ/ظ/g;
    $row[1] =~ s/ā/آ/g;
    $row[4] =~ s/ħ/ح/g;
    $row[4] =~ s/ẓ/ظ/g;
    $row[4] =~ s/ā/آ/g;
    $signature2id{"$row[1]\t$row[3]"}{"$row[4]"} = $row[0];
    while ($row[4] =~ s/^(.+)[،,].*$/\1/) {
      $signature2id{"$row[1]\t$row[3]"}{"$row[4]"} = $row[0];
    }
  }
}
$sth->finish;

$sth = $cdbh->prepare("select id,canform,explid,inflclass,inflforms,syntinfos,file from mslexicon;");
$sth->execute();
while (my @row = $sth->fetchrow_array) {
  for my $i (0..$#row) {
    $row[$i] = Encode::decode('utf8',$row[$i]);
  }
  $row[1] =~ s/ħ/ح/g;
  $row[1] =~ s/ẓ/ظ/g;
  $row[1] =~ s/ā/آ/g;
  $row[4] =~ s/ħ/ح/g;
  $row[4] =~ s/ẓ/ظ/g;
  $row[4] =~ s/ā/آ/g;
  my $line = "";
  if (defined($signature2id{"$row[1]\t$row[3]"})) {
    if (defined($signature2id{"$row[1]\t$row[3]"}{$row[4]})) {
      my $id = $signature2id{"$row[1]\t$row[3]"}{$row[4]};
      $line .= "$row[1]\t$row[3]\t";
      my @validations = ();
      my $result = "";
      for (sort keys %{$id2vals{$id}}) {
	if ($result eq "") {$result = $val2status[$_]}
	elsif ($result ne $val2status[$_]) {$result = "?"}
	push @validations, "$val2status[$_]($val2name[$_]:$val2date[$_])";
      }
      if ($result eq "G") {
	$row[5] =~ s/^(\d+)//;
	$line .= ($1+1000);
      }
      $line .= "$row[5]\t###VAL### ";
      $line .= "$result:";
      $line .= join ",", @validations;
    } else {
      $line .= "$row[1]\t$row[3]\t$row[5]";
      my $tmp = "\t###VAL### ";
      my $vals_for_same_class_in_a_previous_state = join(",",map {join ",", map {"$val2status[$_]($val2name[$_]:$val2date[$_])"} keys %{$id2vals{$signature2id{"$row[1]\t$row[3]"}{$_}}}} keys %{$signature2id{"$row[1]\t$row[3]"}});
      if ($vals_for_same_class_in_a_previous_state =~ /(^|,)G/) {
	$tmp .= "Y:~$vals_for_same_class_in_a_previous_state";
      } elsif ($vals_for_same_class_in_a_previous_state =~ /(^|,)R/) {
	if ($row[1].$row[4] =~ /[حظآ]/) {
	  $tmp = "";
	} else {
	  $tmp .= "R:~$vals_for_same_class_in_a_previous_state";
	  $line = "###### $line";
	}
      } elsif ($vals_for_same_class_in_a_previous_state =~ /(^|,)O/) {
	$tmp .= "O:~$vals_for_same_class_in_a_previous_state";
      } else {
#	$line .= "?:~$vals_for_same_class_in_a_previous_state";
	$tmp = "";
      }
      $line .= $tmp;
    }
  } else {
    $line .= "$row[1]\t$row[3]\t$row[5]";
  }
  print $line."\t$row[6]\n";
}
$sth->finish;

undef $sth;
$dbh->disconnect;

