#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use strict;

my $folder = shift || die "Please provide a folder name";

die "Folder $folder already exists" if (-d $folder);

`mkdir $folder`;

my %filename2content;

while (<>) {
  chomp;
  /^(.*?)\t([^\t]+)$/ || die "Format error: $_";
  $filename2content{$2} .= $1."\n";
}

for (keys %filename2content) {
  open FILE, "> $folder/$_" || die "Could not open $folder/$_: $!";
  binmode FILE, ":utf8";
  print FILE $filename2content{$_};
  close FILE;
}
