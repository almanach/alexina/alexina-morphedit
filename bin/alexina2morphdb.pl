#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
#use strict;
use DBI;

my $lang = "fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-l$/) {$lang = shift}
    else {push (@files, $_); push (@tmpfiles, $_.".morphdb-tmp")}
}

my $db = "$lang.dat";

if (-r $db) {
  print STDERR "Erasing previous database\n";
  `rm $db`;
}

`make $lang.morpho`;

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE mslexicon (id INTEGER, file, canform, explid, inflclass, inflforms, syntcat, syntinfos, weight INTEGER, PRIMARY KEY(id), UNIQUE(id));");
$dbh->do("CREATE TABLE validations (id INTEGER, login, date, status, newsyntcat);");
my $sth=$dbh->prepare('INSERT INTO mslexicon(id, file, canform, explid, inflclass, inflforms, syntcat, syntinfos, weight) VALUES (?,?,?,?,?,?,?,?,?)');
my $vsth=$dbh->prepare('INSERT INTO validations(id, login, date, status) VALUES (?,?,?,?)');

$id = 0;

for $file (@files) {
  $tmpfile = $file.".morphdb-tmp";
  print STDERR "Processing $file\n";
  open FILE, "< $file" || die "Could not open $file: $!";
  open TMPFILE, "> $tmpfile-2" || die "Could not open $tmpfile-2: $!";
  while (<FILE>) {
    chomp;
    next if (/^\s*#/ || /^\s*$/ || /^>+\t/);
    print TMPFILE "$_\n";
  }
  close TMPFILE;
  close FILE;
  my $rads = "";
  $rads = "-rads " if ($lang eq "sk");
  `cat $tmpfile-2 | perl ./morpho.$lang.dir.pl -no_sa -no_sep $rads > $tmpfile-3`;
  `paste $tmpfile-3 $tmpfile-2 > $tmpfile`;
  if ($lang =~ /^(fa|ckb)$/) {
    `/usr/local/share/sxpipe/txt2txt/romanizer.pl -ua -l $lang -2 < $tmpfile > $tmpfile-4 && mv $tmpfile-4 $tmpfile`;
  }
}


for $tmpfile (@tmpfiles) {
  open FILE, "< $tmpfile" || die "Could not open $file: $!";
  $file = $tmpfile;
  $file =~ s/\.morphdb-tmp$//;
  while (<FILE>) {
    chomp;
    /^([^\t]*)\t([^\t]*?(?:___([^\t]*))?)\t([^\t]*?)(?:\t((\d+);.*?;(.*?);.*?(?:###VALDATA### (.*))?|()()().*))?$/ || die "Format error: $_";
    $id++;
    $valdata = $8;
    $sth->execute($id,$file,$2,$3,$4,$1,$7,$5,$6);
    for $vd (split / /, $valdata) {
      $vs =~ /^(.*?);(.*?);(.*?)$/ || die "Wrong format: $_";
      $vsth->execute($id,$1,$2,$3);
    }
  }
  close FILE;
}
$dbh->commit;

$sth->finish;
undef $sth;
$vsth->finish;
undef $vsth;
$dbh->disconnect;

`rm -f *.morphdb-tmp*`;
